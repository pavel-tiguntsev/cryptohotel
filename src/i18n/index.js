Vue.i18n.add("ru", require("./ru.json"));
Vue.i18n.add("en", require("./en.json"));
Vue.i18n.add("es", require("./es.json"));

// set the start locale to use
Vue.i18n.set("ru");

Vue.i18n.fallback("ru");
