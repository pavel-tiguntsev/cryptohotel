// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import * as VueCookie from "vue-cookie";
// import VueI18n from "vue-i18n";
// import "./i18n";

// Vue.use(VueI18n);

Vue.use(VueAxios, axios);
Vue.use(VueCookie);
Vue.config.productionTip = false;

const messages = {
  en: {
    message: {
      hello: "hello world"
    }
  },
  ja: {
    message: {
      hello: "こんにちは、世界"
    }
  }
};

// const i18n = new VueI18n({
//   locale: "fr",
//   messages
// });

/* eslint-disable no-new */
new Vue({
  // i18n,
  el: "#app",
  router,

  components: {
    App
  },
  template: "<App/>"
});
