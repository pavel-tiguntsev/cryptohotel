import Vue from "vue";
import Router from "vue-router";

import landing from "@/components/landing";
import login from "@/components/login";
import signup from "@/components/signup";
import hashrate from "@/components/hashrate";
import miners from "@/components/miners";
import users from "@/components/users";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "landing",
      component: landing
    },
    {
      path: "/login",
      name: "login",
      component: login
    },
    {
      path: "/signup",
      name: "signup",
      component: signup
    },
    {
      path: "/hashrate",
      name: "hashrate",
      component: hashrate
    },
    {
      path: "/miners",
      name: "miners",
      component: miners
    },
    {
      path: "/users",
      name: "users",
      component: users
    }
  ]
});
